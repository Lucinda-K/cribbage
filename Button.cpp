/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

.cpp file for button class
*/

#include "Button.h"
#include <iostream>

SDL_Event eventB;

using namespace std;

Button::Button() { 
	//Sets the button's attributes 
	box.x = 1; 
	box.y = 1; 
	box.w = 1; 
	box.h = 1; 
}

void Button::change_variables(int x_in, int y_in, int w_in, int h_in){
// Allows the button's size constraints to be changed
	box.x=x_in;
	box.y=y_in;
	box.w=w_in;
	box.h=h_in;
}

bool Button::was_pressed(int x, int y){
// Determines whether or not the button was pressed
	if((x>box.x)&&(x<(box.x+box.w))&&(y>box.y)&&(y<(box.y+box.h))) {
	// If the inputted coordinates were within the button's range
		return true;
	}

	// If not within the button's range
	return false;
}
