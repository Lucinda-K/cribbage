all: main

main:	main.o Deck.o Cribbage.o
	g++ -lSDL_ttf main.cpp Deck.cpp Button.cpp Cribbage.cpp -o main

main.o:	main.cpp
	g++ -c main.cpp

Deck.o: Deck.cpp
	g++ -c Deck.cpp

Button.o: Button.cpp
	g++ -c Button.cpp

Cribbage.o:	Cribbage.cpp
	g++ -c Cribbage.cpp

clean:
	rm -f *.o main
	rm -f *~
