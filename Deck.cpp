/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

Deck.cpp implementation

*/



#include <iostream>
#include <deque>
#include <vector>
#include <algorithm>	// random_shuffle
#include <stdlib.h> 	// srand, rand
#include <time.h>	// time

#include <string>
#include "SDL/SDL.h"

#include "Deck.h"

using namespace std;



Deck::Deck(){
// Creates a deck of 52 cards with 4 suits, 
	srand( (time(NULL))); // Gets random shuffle seed for shuffle function, used later
	newDeck();

}

void Deck::newDeck(){

	int rank;

		struct card myCard;	// Declares struct of type card

		for(rank = 1; rank <=13; rank++){	// Steps through ranks 1-13, A-K
			myCard.rank = rank;		// Assigns rank of card, 1-13

			myCard.suit = 'H';
			myDeck.push_back(myCard);	// Adds card object to deque, hearts suit 

			myCard.suit = 'D';
			myDeck.push_back(myCard);	// Adds card object to deque, diamonds suit

			myCard.suit = 'S';
			myDeck.push_back(myCard);	// Adds card object to deque, spades suit

			myCard.suit = 'C';
			myDeck.push_back(myCard);	// Adds card object to deque, clubs suit
		}
	shuffle();
}

int Deck::getSize(){
// Determines and returns number of cards in the deck

	return myDeck.size();	// Uses size() function to access size of private member, myDeck
}

void Deck::printDeck(){
// Prints the deck - rank and suit, i.e. ace of diamonds is "AD"

	deque<card>::const_iterator i;		// Iterator to step through deque
	for(i = myDeck.begin(); i != myDeck.end(); i++){ // Step through deck using iterator

		if(i->rank == 1)
			cout << "A";			// Rank 1 = Ace, A
		else if(i->rank == 11)
			cout << "J";			// Rank 11 = Jack, J
		else if(i->rank == 12)
			cout << "Q";			// Rank 12 = Queen, Q
		else if(i->rank == 13)
			cout << "K";			// Rank 13 = King, K
		else
			cout << i->rank;		// Otherwise, print standard rank (2-10)
		cout << i->suit << ", ";	// Prints suit and comma to separate cards

	}
	cout << endl; // New line for end of printed line

}

void Deck::shuffle(){ // Shuffles deck

	random_shuffle(myDeck.begin(), myDeck.end());		// Shuffles deck

}

void Deck::deal(vector<struct card> &hand){
// Deals one card to the hand/crib
	if(myDeck.size() < 5) {	// if deck is running out of cards...
		newDeck();					// Runs constructor, adds 52 cards and shuffles	
	}
	hand.push_back(myDeck[0]);	// Adds the top card to the hand/crib
	myDeck.pop_front();		// Removes top card from deck
} 


