/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

main.cpp

*/


//#include "Deck.h"
#include "Cribbage.h"

int main(){

	Cribbage CribbageGame;		// declare object, class Cribbage
	CribbageGame.game();		// play the game
	return 0;
}


