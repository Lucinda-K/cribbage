/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

Header file to hold button class
*/

#ifndef BUTTON_H
#define BUTTON_H

#include "SDL/SDL.h"

using namespace std;

class Button { 
	public: 
		// Initializes the variables 
		Button(); 
		
		// Handles events and set the button's sprite region 
		bool was_pressed(int x, int y); 

		// Function to change the pre-initialized variables
		void change_variables(int x_in, int y_in, int w_in, int h_in);
	private: 
		// The attributes of the button 
		SDL_Rect box; 
 };

#endif
