/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

Cribbage.h class
*/

#include <iostream>
#include <deque>
#include <vector>
#include <algorithm>	// Random_shuffle
#include <stdlib.h> 	// srand, rand
#include <time.h>	// Time
#include "Deck.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"  
#include <string>
#include "Button.h" 	// Includes header file for clicking functionality
#include "SDL/SDL_ttf.h"

#ifndef CRIBBAGE_H
#define CRIBBAGE_H


class Cribbage{

public:

	Cribbage();									// Default constructor
   	void load_board();								// Loads images for the board and pegs
	void score_message(const char*, int, const char*, int);
	void set_up_messages();									// loads images for the board and pegs
	void load_message(const char*, int, int, int, int);
	void load_message_black(const char*, int, int, int, int);
	void game();									// Interactive game play
	void DisplayHands(int show);							// Uses SDL to display the hands
	void printCrib();								// Uses SDL to display the crib at the end of the round

	void addToCrib(vector<struct card> &hand, int playernum, int step);		// Prompts user, adds a card to the crib, removes from hand
	void addToPlay(vector<struct card> &hand, int playernum);			// Prompts user, plays card, removes from hand

	// Functions for game play during the play
	void playCards();						// Implements the "play"
	int checkFor31(vector<struct card> &hand);			// Compares player cards to play sum, returns 1 if the player can play
	void newPlay();							// Starts a new play

	// Functions for scoring the "play"
	void scorePlay15(int player);					// Scores the play for 15s
	void scorePlayRuns(int player);					// Scores the play for runs (in order or not)
	int isRun(vector<int> &hand);					// Returns 1 if vector in incremental order, i.e. 3,4,5 NOT 3,4,6
	void scorePlayPairs(int player);				// Scores the play for pairs, triples, etc.
	void scorePlay(int player);					// Calls all the scoring functions for the play

	void returnCards();						// Returns cards from the play to the players

	// Functions for scoring the hands and crib at the end of each turn
	int score_fifteens(vector<struct card> &hand);		// Checks to see all possible 15s for a HAND
	int already_paired(int, int, vector<int> &hand); 	// Stores already paired cards
	int score_hand_doubles(vector<struct card> &hand);	// Checks for pairs in hand
	int hand_score_runs(vector<struct card> &hand);
	int hand_score_flush(vector<struct card> &hand);
	int score_hand_starter(vector<struct card> &hand);	// Returns number of points for "his nobs", aka. jack of the same suit as starter card
	void scoreHands();					// Scores both players' hands at end of each turn
	void update_board(int, int);		
	void changeScore(int *score, int change);		// Updates the score
				
	int click_card();					// Function to enable card click choosing
	void endGame();

private:

	Deck CribbageDeck;					// Deck of cards, composition with the Deck class
	int dealer;							// Always 1 or 2, determines which player is the dealer
	int player;							// Always 1 or 2, opposite of the dealer
	int maxScore;						// Ending score to stop game

	// Variables for the crib, hands, and scores
	vector<struct card> hand1;				// Player 1's hand
	vector<struct card> hand2;				// Player 2's hand
	vector<struct card> *dealerHand;		// Pointer for the dealer hand
	vector<struct card> *playerHand;		// Pointer for the non-dealer hand
	int score1;						// Player 1's score
	int score2;						// Player 2's score
	int *dealerScore;				// Pointer for the dealer score
	int *playerScore;				// Pointer for the non-dealer score

	vector<struct card> crib;			// Crib
	vector<struct card> play;			// The play
	vector<struct card> starter;			// Starter card
	
	int playSum;					// Keeps track of sum of play (can't exceed 31)
	int playChange;					// Keeps track of where a new play starts


	// Variables for card positions: used in display functions
	int crib_y;					// y position of crib cards
	int c_x;					// First x position of crib cards
	
	int p1_y;					// y position of player 1 hand
	int p2_y;					// y position of player 2 hand
	int p_x;					// First x position of player hands (hands are aligned)
	
	int cp_y;					// y position of first card played
	int cp_x;					// x position of first card played

	vector<int> player_x;				// Vector to hold players' cards' x positions
	vector<int> crib_x;				// Vector to hold cribs' cards' x positions
	vector<int> cardplay_x;				// Vector to hold played cards' x positions
	vector<int> cardplay_y;				// Vector to hold played cards' y positions

	int playPos;					// Index for played card position

	// SDL Surfaces
	SDL_Surface* board; 
	SDL_Surface* peg1; 
	SDL_Surface* peg2;
	SDL_Surface* message;
	TTF_Font *font;

	//For button functionality
	Button hand1_0;
	Button hand1_1;
	Button hand1_2;
	Button hand1_3;
	Button hand1_4;
	Button hand1_5;

	Button hand2_0;
	Button hand2_1;
	Button hand2_2;
	Button hand2_3;
	Button hand2_4;
	Button hand2_5;

};

#endif
