/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

Header file to hold card display functions
*/


#ifndef CARDDISPLAY_H
#define CARDDISPLAY_H

#include <iostream>
#include <deque>
#include <vector>
#include <algorithm>	// Random_shuffle
#include <stdlib.h> 	// srand, rand
#include <time.h>	// Time

#include <string>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"  


const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 900;
const int SCREEN_BPP = 32;

// Initializes surface pointers
SDL_Surface *card = NULL;
SDL_Surface *background = NULL;
SDL_Surface *screen = NULL;
SDL_Surface *blank = NULL;

// Defines event structure
SDL_Event event;

// Defines position parameters
const int crib_y=30;	// y position of crib cards
const int c_x=790;	// First x position of crib cards

const int p1_y=50;	// y position of player 1 hand
const int p2_y=350;	// y position of player 2 hand
const int p_x=15;	// First x position of player hands (hands are aligned)

const int cp_y=100;	// y position of first card played
const int cp_x=500;	// x position of first card played


SDL_Surface *load_image( string filename)
{
	// Temporary storage for loaded image
	SDL_Surface* loadedImage = NULL;

	// Optimized image to be used
	SDL_Surface* optimizedImage = NULL;

	// Loads image
	loadedImage = SDL_LoadBMP( filename.c_str() );

	if( loadedImage != NULL ){
		optimizedImage = SDL_DisplayFormat( loadedImage );
	
		SDL_FreeSurface( loadedImage );
	}

	return optimizedImage;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination ) {
	// Temporary rectangle to hold offsets
	SDL_Rect offset;

	// Gives offsets to rectangle
	offset.x = x;
	offset.y = y;

	SDL_BlitSurface( source, NULL, destination, &offset );
}

bool init() {
// Intializes all SDL library
	if (SDL_Init(SDL_INIT_EVERYTHING ) == -1 ) {
		return 1;
	}

	// Creates screen based on preset parameters
	screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

	// Makes sure screen initialized correctly
	if (screen == NULL ) {
		return false;
	}

	// Sets caption for top of screen
	SDL_WM_SetCaption("Cribbage Game", NULL);

	return true;
}

bool load_files(char suit, int rank) {
// Determines which image to load, and loads into the correct SDL surface
	background=load_image("cardimages/Back_3.bmp");

	switch (rank) {
		case 1:
			if(suit=='H') {
				card=load_image("./cardimages/001h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/001d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/001s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/001c.bmp");
			}
			break;
		case 2:
			if(suit=='H') {
				card=load_image("./cardimages/002h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/002d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/002s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/002c.bmp");
			}
			break;
		case 3:
			if(suit=='H') {
				card=load_image("./cardimages/003h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/003d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/003s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/003c.bmp");
			}
			break;
		case 4:
			if(suit=='H') {
				card=load_image("./cardimages/004h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/004d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/004s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/004c.bmp");
			}
			break;
		case 5:
			if(suit=='H') {
				card=load_image("./cardimages/005h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/005d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/005s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/005c.bmp");
			}
			break;
		case 6:
			if(suit=='H') {
				card=load_image("./cardimages/006h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/006d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/006s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/006c.bmp");
			}
			break;
		case 7:
			if(suit=='H') {
				card=load_image("./cardimages/007h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/007d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/007s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/007c.bmp");
			} 
			break;
		case 8:
			if(suit=='H') {
				card=load_image("./cardimages/008h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/008d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/008s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/008c.bmp");
			}
			break;
		case 9:
			if(suit=='H') {
				card=load_image("./cardimages/009h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/009d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/009s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/009c.bmp");
			}
			break;
		case 10:
			if(suit=='H') {
				card=load_image("./cardimages/010h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/010d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/010s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/010c.bmp");
			}
			break;
		case 11:
			if(suit=='H') {
				card=load_image("./cardimages/011h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/011d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/011s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/011c.bmp");
			}
			break;
		case 12:
			if(suit=='H') {
				card=load_image("./cardimages/012h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/012d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/012s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/012c.bmp");
			}
			break;
		case 13:
			if(suit=='H') {
				card=load_image("./cardimages/013h.bmp");
			} else if (suit=='D') {
				card=load_image("./cardimages/013d.bmp");
			} else if (suit=='S') {
				card=load_image("./cardimages/013s.bmp");
			} else if (suit=='C') {
				card=load_image("./cardimages/013c.bmp");
			}
			break;
		case 0:
			// Loads the back of the card - the upside down card image
			card=load_image("./cardimages/Back_3.bmp");
			break;
		default:
			cout << "load images error - invalid card rank" << endl;
			break;
	}
	// Makes sure all initialized correctly
	if( card==NULL || background==NULL) {
		return false;
	}

	return true;
}

void clean_up() {
// Function to free allocated surfaces
	SDL_FreeSurface(card);
	SDL_FreeSurface(background);

	// Quits SDL
	SDL_Quit();
}

void FillRect(int x,int y,int w, int h, int color) {
// Function to draw a solid rectangle in SDL
	SDL_Rect rect={x,y,w,h};
	SDL_FillRect(screen,&rect,color);
}

bool DisplayCard(int rank, char suit) {
// Function to display a card
	if( init() == false){
		cout << "error init ";
		return false;
	}
	if(load_files(suit,rank)==false) {
		cout << "error load ";
		return false;
	}
	apply_surface(180,140,card,screen);
	if( SDL_Flip(screen)==-1) {
		cout << "error flip ";
		return false;
	}
	return true;
}

#endif
