/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

Deck.h class

*/

#ifndef DECK_H
#define DECK_H

#include <iostream>
#include <deque>
#include <vector>

using namespace std;

struct card{

	int rank;
	char suit;
};

class Deck{

public:

	Deck();				// Initializes deque of 52 card structs, with rank and suit
	int getSize();			// Returns size of deck
	void printDeck();		// Prints the rank and suit of each card in the deck
	void shuffle();			// Shuffles the deck
	void deal(vector<card> &hand);	// Deals card to a hand/crib
	void newDeck();

private:

	deque<card> myDeck;		// Deque of card structs

};

#endif
