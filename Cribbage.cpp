/* Lucinda Krahl, James Bowyer, Tabitha Ricketts
CSE20212 Fundamentals of Computing II
Final project - Cribbage

Cribbage.cpp implementation
*/

#include <iostream>
#include <deque>
#include <vector>
#include <algorithm>	// random_shuffle, sort
#include <stdlib.h> 	// srand, rand
#include <time.h>		// time
#include "Deck.h"
#include "Cribbage.h"
#include "math.h"
#include "CardDisplay.h"
#include <string>
#include <sstream>

Cribbage::Cribbage(){
// default constructor - initializes scores and sums to 0
// sets up screen and card positions 

	// Initialize variables
	playSum = 0;
	score1 = 0;
	score2 = 0;
	playChange = 0;
	dealer = 1;
	player = 2;
	maxScore = 61;

	crib_y=30;	// y position of crib cards
	c_x=790;	// first x position of crib cards
	
	p1_y=50;	// y position of player 1 hand
	p2_y=350;	// y position of player 2 hand
	p_x=15;	// first x position of player hands (hands are aligned)
	
	cp_y=100;	// y position of first card played
	cp_x=500;	// x position of first card played

	playPos=0;	// Initialize played-card position index

	for(int i=0;i<8;i++){
		cardplay_y.push_back(cp_y+(40*i));
		cardplay_x.push_back(cp_x+(45*i));
		// For 8 card positions, establish positions for card play
		if(i<=5){
		// For 6 card positions, establish card-spaced x positions for player hands
			player_x.push_back(p_x+(108*i));
		}
		if(i<=3){
		// Likewise for the crib, but only for 4 cards
			crib_x.push_back(c_x+(108*i));
		}
	}

	// Initialize display screen
	if( init() == false){
		cout << "error init ";
	}

	// Initialize blank card image
	blank=load_image("./cardimages/Back_3.bmp");
	if(!blank){
		cout << "error loading card-back";
	}

	// Create card position buttons
	int cardw=109;	// Establish card height and width
	int cardh=169;
		// Instantiate the buttons
	hand1_0.change_variables(player_x[0],p1_y,cardw,cardh);
	hand1_1.change_variables(player_x[1],p1_y,cardw,cardh);
	hand1_2.change_variables(player_x[2],p1_y,cardw,cardh);
	hand1_3.change_variables(player_x[3],p1_y,cardw,cardh);
	hand1_4.change_variables(player_x[4],p1_y,cardw,cardh);
	hand1_5.change_variables(player_x[5],p1_y,cardw,cardh);

	hand2_0.change_variables(player_x[0],p2_y,cardw,cardh);
	hand2_1.change_variables(player_x[1],p2_y,cardw,cardh);
	hand2_2.change_variables(player_x[2],p2_y,cardw,cardh);
	hand2_3.change_variables(player_x[3],p2_y,cardw,cardh);
	hand2_4.change_variables(player_x[4],p2_y,cardw,cardh);
	hand2_5.change_variables(player_x[5],p2_y,cardw,cardh);

	//endGame();
}

void Cribbage::load_board(){
// Load images for the peg board
  board = load_image( "./crib-board.bmp" );
  peg1 = load_image( "./test-peg.bmp");
  peg2 = load_image( "./test-peg2.bmp");
}

void Cribbage::load_message(const char* sun, int x, int y, int color, int size){
	if( TTF_Init() == -1 ) { 
	      cout<<"Init of messages failed"<<endl; 
	}
	SDL_Color textColor = { 255, 255, 255 };
	if(color==1) {
		SDL_Color textColor = { 255, 255, 255 };
	}
	else{
		SDL_Color textColor = { 0, 0, 0 };
	}
	font = TTF_OpenFont( "Ubuntu-Title.ttf", size );
	message = TTF_RenderText_Solid( font, sun, textColor );
	apply_surface( x, y, message, screen );
	SDL_Flip( screen );
}

void Cribbage::load_message_black(const char* sun, int x, int y, int color, int size){
	if( TTF_Init() == -1 ) { 
	      cout<<"Init of messages failed"<<endl; 
	}
	SDL_Color textColor = { 0, 0, 0 };
	font = TTF_OpenFont( "Ubuntu-Title.ttf", size );
	message = TTF_RenderText_Solid( font, sun, textColor );
	apply_surface( x, y, message, screen );
	SDL_Flip( screen );
}

void Cribbage::set_up_messages(){
	const char* message;
	message="cribbage";
	load_message(message, 550, 0, 1, 45);
	message="player 1";
	load_message(message, 20, 0, 1, 45);
	message="player 2";
	load_message(message, 20, 295, 1, 45);
	message="crib";
	load_message(message, 980, 200, 1, 45);
	message="Player 1: ";
	load_message(message,50,815,1,40);
	message="Player 2: ";
	load_message(message,325,815,1,40);
	message="play count: ";
	load_message(message,260,250,1,40);
/*
				FillRect(450,275,100,100,1);
				stringstream playSumstr;
				playSumstr<<playSum;
				string str1 = playSumstr.str();
				char* playSum_str = (char*)str1.c_str();
				load_message(playSum_str,450,275,1,40);
*/
	changeScore(playerScore,0);			// set scores to 0
}

void Cribbage::game(){				
// Full interactive game play - the ONLY function that needs to be run in the main.cpp file

		FillRect(450,250,100,100,1);
		stringstream playSumstr;
		playSumstr<<playSum;
		string str1 = playSumstr.str();
		char* playSum_str = (char*)str1.c_str();
		load_message(playSum_str,450,250,1,40);

	// Initialize game variables and board 
	load_board();
	const char* message;
	update_board(0, 0); 			// SDL START/END
	int dealer_turn = 0;			// Used to keep track of dealer/non-dealer, incrememtned after each turn
	int sdlquit = 0;

	// Display the deck card - card back
	if(load_files(0,0)==false){
		cout << "error load" << endl;
	}
	apply_surface(950,600,card,screen);
	SDL_Flip(screen);

	int cont = 1;
	const char* dealer_indicator = "< dealer";		// indicates who the dealer is

	while(score1 < maxScore && score2 < maxScore)		// will change, 40 is for practice purposes ////////////////
	{	// This loop runs as long as nobody has won (no one has reached 61 points);
		set_up_messages();

		// Keeps track of dealer and non-dealer, switches after each turn
		// Uses modular math, dealer_turn is incremented after each turn
		dealer = (dealer_turn%2)+1;			// Will always be 1 or 2 (because the dealer is player 1 or player 2)
		player = dealer%2 + 1;				// Will always be 1 or 2, the OPPOSITE of dealer

		if(dealer==1){							// Dealer = player 1
			dealerHand = &hand1;					// Make dealer pointer point to player 1's hand
			playerHand = &hand2;					// Make player pointer point to player 2's hand

			dealerScore = &score1;					// Make dealer pointer point to player 1's score
			playerScore = &score2;					// Make player pointer point to player 2's score

			load_message(dealer_indicator, 175, 10, 1, 35);
		}
		if(dealer==2){							// Dealer = player 2
			dealerHand = &hand2;					// Make dealer pointer point to player 1's hand
			playerHand = &hand1;					// Make player pointer point to player 2's hand

			dealerScore = &score2;					// Make dealer pointer point to player 1's score
			playerScore = &score1;					// Make player pointer point to player 2's score

			load_message(dealer_indicator, 175, 305, 1, 35);
		}

		/* FROM THIS POINT FORWARD, hand1 and hand2 are referenced and modified 
		   using the pointers: *dealerHand, *playerHand */

		/* FROM THIS POINT FORWARD, score1 and score2 are referenced and modified 
		   using the pointers: *dealerScore, *playerScore */


		// DEAL CARDS
		for(int i = 0; i<6; i++){				// Deal 6 cards to both player's hands
				CribbageDeck.deal(*playerHand);			// Non-dealer dealt first
				CribbageDeck.deal(*dealerHand);			// Dealer dealt second
		}


		// DISPLAY HANDS
		DisplayHands(0);		// Displays both players hands on screen as blank cards


		// ADD TO CRIB - 2 per player
		//  Non-dealer goes first
		// 	Waits for click, then display the correct player's hand
		int quit=0;
		while(!quit){
			if(SDL_PollEvent(&event)){
				if( event.type == SDL_MOUSEBUTTONDOWN ) {
					DisplayHands(player);
					quit=1;
				}
			}
		}

		// 	Adds 2 of non-dealer's cards to the crib
		for(int crib1 = 0; crib1<2; crib1++){
			addToCrib(*playerHand,player,crib1);			
		}

		// 	After player adds to crib, waits ~1/3 a second to show new hand
		SDL_Delay(300);
		//	Then flips all cards back over for next player's turn
		DisplayHands(0);


		//  Dealer goes second
		//	Waits for click, then display the correct player's hand
		quit=0;
		while(!quit){
			if(SDL_PollEvent(&event)){
				if( event.type == SDL_MOUSEBUTTONDOWN ) {
					DisplayHands(dealer);
					quit=1;
				}
			}
		}

		// 	Adds 2 of dealer's cards to the crib
		for(int crib2 = 0; crib2<2; crib2++){				
			addToCrib(*dealerHand,dealer,crib2);	
		}

		// 	After player adds to crib, waits ~1/3 a second to show new hand
		SDL_Delay(300);
		//	Then flips all cards back over for next player's turn
		DisplayHands(0);


		// DEAL STARTER CARD
		CribbageDeck.deal(starter);

		//	Loads and displays starter card, dealt immediately after the crib has been filled
		if(load_files(starter[0].suit,starter[0].rank)==false){
			cout << "error load" << endl;
		}
		apply_surface(800,600,card,screen);
		SDL_Flip(screen);

		//	Checks starter card: if it's a jack, give the dealer two pints
		if(starter[0].rank==11){
			changeScore(dealerScore,2);
			score_message("His heels: ",2,"Starter for player ",dealer);

		}
		if(score1>=maxScore || score2>=maxScore){
			endGame();			
		}


		// PLAY
		playCards();				// implements entire game play for the "play" part of the game


		// RETURN CARDS
		returnCards();				// returns cards to the players' hands to be scored


		// SCORE THE HANDS
		scoreHands();				// changes the scores for both players

		//	Empties the hands and crib (discard cards)
		crib.clear();				// clear crib
		hand1.clear();				// clear hand
		hand2.clear();				// clear hand
		starter.clear();			// clear starter 
		playSum = 0;				// reset sum
		playChange = 0;				// reset change index
		playPos=0;				// reset played card position index
		dealer_turn++;				// increment dealer_turn to switch dealers

		// 	Holds the screen after everything is played, clears when clicked
		quit=0;
		while(!quit){
			if(SDL_PollEvent(&event)){
				if( event.type == SDL_MOUSEBUTTONDOWN ) {
					quit=1;
				}
			}
		}

		// Clears graphics screen
		FillRect(0,0,1280,600,1);	// Top 2/3 of screen (must not clear score board!!)
		FillRect(800,600,106,169,1);	// Starter card

		if( SDL_Flip(screen)==-1) {	// Updates screen
			cout << "error flip";
		}
		SDL_Delay(500);			// Waits 1/2 seconds before beginning next round
	}
}	// End game()

void Cribbage::score_message(const char* message, int score, const char* hand, int player){
// displays scoring message if a player earns points
// shows what player is being scored, what type, and how many points
	stringstream sstr;						
	sstr<<score;
	string str1 = sstr.str();
	char* scorestr=(char*)str1.c_str();			// change score to string

	stringstream sstr2;
	sstr2<<player;
	string str2 = sstr2.str();
	char* playerstr=(char*)str2.c_str();		// change player number to string

	load_message(hand, 900, 300, 1, 33);		//display which player is being scored
	load_message(playerstr, 1225, 300, 1, 33);
	load_message(message, 1000, 350, 1, 33);	// display message for type
	load_message(scorestr, 1225, 350, 1, 33);	// display points scorecd
	SDL_Delay(1500);							// wait
	// black out messages
	load_message_black(message, 1000, 350, 0, 33);		
	load_message_black(scorestr, 1225, 350, 0, 33);
	load_message_black(hand, 900, 300, 0, 33);
	load_message_black(playerstr, 1225, 300, 0, 33);
}

void Cribbage::DisplayHands(int show){
// Displays the players' hands as initially dealt

	int size1 = hand1.size();
	int size2 = hand2.size();
	int i,j;

	if (show==0) {
		// Displays both hands of blank cards!
		for(j=0;j<size1;j++){ // Player 1's hand
			apply_surface(player_x[j],p1_y,blank,screen);
		}
		for(i=0;i<size2;i++){ // Player 2's hand
			apply_surface(player_x[i],p2_y,blank,screen);
		}
		
	} else if (show==1) {	
		// Displays player 1's hand only
		for(i = 0; i<size1; i++){
			if(load_files(hand1[i].suit,hand1[i].rank)==false){
				cout << "error load" << endl;
			}
			apply_surface(player_x[i],p1_y,card,screen);
		}
	} else if (show==2) {
		// Displays player 2's hand only
		for(j = 0; j<size2; j++){
			if(load_files(hand2[j].suit,hand2[j].rank)==false){
				cout << "error load" << endl;
			}
			apply_surface(player_x[j],p2_y,card,screen);
		}
	}

	// Update screen
	if( SDL_Flip(screen)==-1) {
		cout << "error flip"<<endl;
	}
}



void Cribbage::addToCrib(vector<struct card> &hand, int playernum, int step){
// Takes user input to select card to put in crib
// Places card in the crib and removes from hand

	int cribCardIndex = 10;
	int check = 1;
	int yUse, stepCrib;

	// Determines coordinates to use, for player 1 or 2's cards
	switch(playernum){
		case 1:
			yUse=p1_y;
			stepCrib=step;
			break;
		case 2:
			yUse=p2_y;
			stepCrib=step+2;
			break;
	}

	while(check){		// Repeats if user gives invalid index

		cribCardIndex=click_card();
		
		if(cribCardIndex<hand.size() && cribCardIndex>=0){
			crib.push_back(hand[cribCardIndex]);		// Adds card to crib

			check = 0;	// Exit while loop - valid index was given

			FillRect(player_x[5-step],yUse,106,169,1);		// Erases the card on the end
			apply_surface(crib_x[0+stepCrib],crib_y,blank,screen);	// Puts upside-down card into crib position
			if( SDL_Flip(screen)==-1) {				// Applies to screen
				cout << "error flip ";
			}
			hand.erase(hand.begin()+cribCardIndex);			// Removes card from hand
			DisplayHands(playernum);				// Displays new hand with card removed
		}

		else{		// If invalid index... loops back through while
			// No valid card ... does nothing
		}
	}
}

void Cribbage::printCrib() {
	// Displays each card in the crib
	for(int i=0; i<crib.size(); i++) {
		if(load_files(crib[i].suit,crib[i].rank)==false){
			cout<<"error print crib load";
		}
		apply_surface(crib_x[i],crib_y,card,screen);
	}

	// Updates screen
	if(SDL_Flip(screen)==-1) {
		cout << "error print crib flip";
	}
}

void Cribbage::addToPlay(vector<struct card> &hand, int playernum){
// Takes user input to select card to put in crib
// Places card in the play and removes from hand

	int playCardIndex = 10;		// Input from player, indicates which card to play
	int check = 1;
	int cardValue, yUse;

	// Determines coordinates to use, for player 1 or 2's cards
	if(playernum==1){
		yUse=p1_y;
	} else if(playernum==2){
		yUse=p2_y;
	}

	while(check){
		// Waits for user click before displaying the player's hand for them to take their turn
		int quit=0;
		while(!quit){
			if(SDL_PollEvent(&event)){
				if( event.type == SDL_MOUSEBUTTONDOWN ) {
					DisplayHands(playernum);
					quit=1;
				}
			}
		}

		// Allows user to click a card to choose
		playCardIndex=click_card();

		// If the chosen card is a valid card to play (it exists in the player's hand)
		if(playCardIndex<hand.size() && playCardIndex>=0){
			if(hand[playCardIndex].rank>10) {
				cardValue = 10;				// Changes J,Q,K card value to 10
			} 
			else {
				cardValue = hand[playCardIndex].rank;	// Sets value to card rank
			}
			if((playSum + cardValue) > 31)	{		// Makes sure it won't exceed 31
				// INVALID input - does nothing
			} 
			else {						// If it does NOT exceed 31...
				playSum+=cardValue;				// Increments play sum
	/*			
	message="player 2";
	load_message(message, 20, 295, 1, 45);
*/
				FillRect(450,250,100,100,1);
				stringstream playSumstr;
				playSumstr<<playSum;
				string str1 = playSumstr.str();
				char* playSum_str = (char*)str1.c_str();
				load_message(playSum_str,450,250,1,40);

				play.push_back(hand[playCardIndex]);		// Adds card to play

				// Loads files to print correct cards
				if(load_files(hand[playCardIndex].suit,hand[playCardIndex].rank)==false){
					cout << "error load";
				}
				FillRect(player_x[hand.size()-1],yUse,106,169,1);				// Erases card on end
				apply_surface(cardplay_x[playPos],cardplay_y[playPos],card,screen);		// Updates position

				hand.erase(hand.begin()+playCardIndex);		// Removes card from hand
				DisplayHands(playernum);
				// Waits ~1/3 a second, displaying player's new hand
				SDL_Delay(300);
				// Then flips the player's hand back over
				DisplayHands(0);

				check = 0;
				playPos++;	// Increments played-card position index
			}
		}
		else{		// If invalid index
			//INVALID - does nothing
		}
	}
}


void Cribbage::playCards(){
// Implements the "play" part of the game

	while(hand1.size()>0 && hand2.size()>0){
	// Switches between players to add cards to play
	// 	Should do this 4 times 

		// NON-DEALER GOES FIRST
		if(checkFor31(*playerHand)){			// Checks for valid card
			// If there's a valid card
			addToPlay(*playerHand,player);		// Plays card
		}
		else{						// If there is no valid card...
			if(playSum!=31){
				changeScore(dealerScore,1);	// Adds 1 point to dealer's score
				score_message("Last card: ",1,"Scoring play for player ",dealer);
			}
			if(score1>=maxScore || score2>=maxScore){
				endGame();		
			}

			newPlay();				// New play
			addToPlay(*playerHand,player);		// Prompts player to play a card (on new play)
		}
		
		scorePlay(player);				// Scores for non-dealer

		// DEALER
		if(checkFor31(*dealerHand)){			// Checks for valid card
			// If there's a valid card
			addToPlay(*dealerHand,dealer);		// Prompts dealer to play a card (on new play)
		}
		else{						// If there is no valid card...
			if(playSum!=31){
				changeScore(playerScore,1);	// Adds 1 point to player 2 score
				score_message("Last card: ",1,"Scoring play for player ",player);
			}
			if(score1>=maxScore || score2>=maxScore){
				endGame();		
			}
			newPlay();				// New play
			addToPlay(*dealerHand,dealer);		// Prompts player 1 to play a card (on new play)
		}
		
		scorePlay(dealer);				// Scores for player 2
	}
	// Updates score
	changeScore(dealerScore,1);
	score_message("Last card: ",1,"Scoring play for player ",1);	
	if(score1>=maxScore || score2>=maxScore){
		endGame();		
	}
}

int Cribbage::checkFor31(vector<struct card> &hand){
// Returns 1 if the player CAN play without exceeding 31

	int cardValue;
	for(int i = 0; i<hand.size(); i++){		// Steps through hand
	// Returns a 1 if at any point a valid card is encountered
		if(hand[i].rank>10)
			cardValue = 10;			// Changes J,Q,K card value to 10
		else
			cardValue = hand[i].rank;	// Sets value to card rank

		if((playSum + cardValue) <= 31)		// If sum WOULD be under 31, return a 1
			return 1;
	}
	return 0;					// Only returns 0 if NO valid card was encountered
}

void Cribbage::newPlay(){
// Starts a new play
		playSum = 0;					// Resets playSum
		playChange = play.size();			// Stores index for where new play starts

		FillRect(450,250,100,100,1);					// cover up old play count
		stringstream playSumstr;
		playSumstr<<playSum;
		string str1 = playSumstr.str();
		char* playSum_str = (char*)str1.c_str();		// change int to string
		load_message(playSum_str,450,250,1,40);			// display play count

		// NOT QUITE WORKING
		const char* message = "New play!";
		load_message(message, 550, 500, 1, 50);
		SDL_Delay(1000);
		load_message_black(message, 550, 500, 0, 50);

		for(int i=0; i<playChange; i++) {
				FillRect(cardplay_x[i],cardplay_y[i],106,169,1);	// Erases the card on the end
		}

		if( SDL_Flip(screen)==-1) {						// Updates screen
			cout << "error flip ";
		}

		playPos=0;	// Resets played-card position index, so the next play will start over at card play position 0
}

void Cribbage::scorePlay15(int player){
// adds points if the most recent "play" completed a 15

	int sum = 0;		// for summing the cards
	int i = 0;			// for stepping through deck
	int rank;			// for value of each card

	while(sum<=15 && i<play.size()){	

		// steps through each card in the play to check for 15s
		if((play.size()-i-1)<playChange)
			break;									// breaks loop when sum surpasses 15 or if it goes out of bounds of the play
		rank = play[play.size()-i-1].rank;			// get rank of card, starts at back = most recent played card
		if(rank>10){
			rank = 10;						// set J,Q,K rank to 10, for adding purposes
		}
		sum+=rank;							// add to sum
		if(sum==15){			
			if(player==1){					// add 2 points to player's score
				changeScore(&score1,2);
				score_message("Fifteens: ",2,"Scoring play for player ",1);
			}
			else{
				changeScore(&score2,2);
				score_message("Fifteens: ",2,"Scoring play for player ",2);
			}
			if(score1>=maxScore || score2>=maxScore){
				endGame();					// endGame if maxScore reached
			}
		}
		i++;
	}
}

void Cribbage::scorePlayRuns(int player){
// Adds points for runs during the play. i.e. 3,4,5 = 3 points, 5,6,7,8 = 4 points
// 	Order does NOT matter


	vector<int> runHand;				// Dummy vector to sort and evaluate for a run in ANY order
	int i = 0;							// For stepping through play vector
	int runs = 0;						// For keeping track of the number of run
	int stopRun = 0;					// Check value
	vector<int>::const_iterator k;		// Iterator to go through deck

	int playSize = play.size() - playChange;				// Size of current play

	if(playSize>=3){										// Can't have run with less than 3 cards
		runHand.push_back(play[play.size()-1].rank);		// Always adds last card
		runHand.push_back(play[play.size()-2].rank);		// Always adds second-to-last card
		for(int j = 2; j<playSize; j++){					// Adds cards, one at a time, starting with third
			runHand.push_back(play[play.size()-1-j].rank);	// Adds to dummy vector
			if(isRun(runHand)){								// Increment runs if run encountered
				runs = runHand.size();
			}
		}								// If run broken, NOTHING happens except runs isn't incremented
	}
	if(runs>=3){		// If there was a run
		if(player==1){
			changeScore(&score1,runs);	// Changes score
			score_message("Runs: ",runs,"Scoring play for player ",1);
		}
		else{
			changeScore(&score2,runs);
			score_message("Runs: ",runs,"Scoring play for player ",2);
		}
		if(score1>=maxScore || score2>=maxScore){
			endGame();					// endGame if maxScore reached
		}
	}
	runHand.clear();
}

int Cribbage::isRun(vector<int> &hand){
// Returns a 1 if the cards in the vector are no more than 1 apart, i.e. 3,4,5 NOT 3,4,6

	int size = hand.size();
	sort(hand.begin(),hand.end());		// Sorts the hand, ascending order
	for(int i = 0; i<(size-1); i++){	// Goes to second-to-last card because it compares i and i+1 (don't want to go out of bounds)
		if((hand[i]+1)!=hand[i+1])	// If the 2 cards in a row aren't 1 apart, i.e. 2,2 or 2,4
			return 0;
	}
	return 1;				// Will only return 1 if it never encounters non-incrememtned cards

}

void Cribbage::scorePlayPairs(int player){
// Adds points for kinds in the play. i.e. 4,4 = 2; 9,9,9 = 3

	int kind = 1;
	int points;
	for(int i = 1; i<=play.size(); i++){ 			// Steps through whole deck or until non-match
		// Compares last card to 2nd to last, last card to 3rd to last, last card to 4th to last, etc.
		// 	Since there are only 4 of every suit, 4 is the MAX number of times this loop could run

		if(play[play.size()-1].rank == play[play.size()-i-1].rank && (play.size()-i-1)>=playChange){	// If the ranks match and it is in the current play...
			kind++;		// Increments to 2 for pair, 3 for triple, 4 for 4-of-a-kind
		}	
		else
			break;		// Breaks loop if non-matching rank encountered or if it goes past the current play
	}
		switch(kind){	// Assigns the right number of points for doubles, triples, etc.
			case 2:
				points = 2;		// 2 card = 2 pair
				break;
			case 3:
				points = 6;		// 3 card = 3 pairs
				break;
			case 4:
				points = 12;	// 4 cards = 6 pairs
				break;
			default:			// CANNOT have 5 cards - only 4 of each rank
				points = 0;
				break;
		}
		if(player==1 && points!=0){				// change scores
			changeScore(&score1,points);		// add to player 1's score
			score_message("Pairs: ",points,"Scoring play for player ",1);
		}
		if(player==2 && points!=0){
			changeScore(&score2,points);		// add to player 2's score
			score_message("Pairs: ",points,"Scoring play for player ",2);
		}
		if(score1>=maxScore || score2>=maxScore){
			endGame();		
		}
}

void Cribbage::scorePlay(int player){
// Scores the play and increments the player's points as it goes

	// Pointers to indicate the correct score
	int *score1ptr = &score1;
	int *score2ptr = &score2;

	// Scoring
	scorePlayPairs(player);
	scorePlay15(player);
	scorePlayRuns(player);

	// Checking sum
	if(playSum==31){			// If sum is 31...
		if(player==1){
			changeScore(score1ptr,2);	// give 2 points to player 1
			score_message("31 : ",2,"Scoring play for player ",1);
		}
		if(player==2){
			changeScore(score2ptr,2);	// give 2 points to player 2
			score_message("31 : ",2,"Scoring play for player ",2);
		}
		if(score1>=maxScore || score2>=maxScore){
			endGame();					// endGame if maxScore reached
		}
	}
}

void Cribbage::returnCards(){	
// Returns cards to the players' hands after the play

	int size = play.size();		// Gets starting size of play (should be twice the size of a hand)
	for(int i=0;i<size;i+=2){
		// Alternates between players 
		(*playerHand).push_back(play[0]);		// Returns card to non-dealer's hand
		play.erase(play.begin());				// 	Removes card from the play
		(*dealerHand).push_back(play[0]);		// Returns card to dealer's hand
		play.erase(play.begin());				// 	Removes card from the play
	}
}

int Cribbage::score_fifteens(vector<struct card> &hand) {
// Returns total number of points earned from 15's

	int i;
	int numofCards=hand.size();
	int points;


	// Declares the bitwise (POWER OF TWO) comparison for the for loop
	vector <int> card_slot;
	card_slot.resize(numofCards);
	for(i=0; i<numofCards; i++)                     // For loop to iniate card_slot vector with powers of two
	{						// *See below for hard code example*
		card_slot[i]=(pow(2, i));
	}

	// For loop to compare all versions of the hand, for 5 card hand 33
	int sum=0;
	int j;
	int fifteens=0;
	int tot=0;

	for(i=0; i<pow(2, card_slot.size()); i++)
	{	
		tot=tot+1;
		for(j=0; j<card_slot.size(); j++) 	// For loop to check all possible hand values
		{									// Uses bitwise math, if the BINARY values put out true
			if(i & card_slot[j])			// 4 and 5 are good 4 and 2 are not
			{	
				if(hand[j].rank>10)
				{
					sum=sum+10;
				} 	
				else
				{							// See below for hard code
				    sum=sum+hand[j].rank;
				}
			}
		}

		if(sum==15)
		{
			fifteens=fifteens+1;
			sum=0;
		}
		else
		{
			sum=0;
		}
	}

	points = fifteens*2;
	return points;
}


int Cribbage::already_paired(int j, int k, vector<int> &paired) {
// Returns 1 if that pair has already been paired

	int i;
	for(i=0; i<paired.size(); i++)
	{
		if(j==paired[i+1] && k==paired[i])
			{ 
			return 1;
			}
	}
	paired.push_back(j);
	paired.push_back(k);
	return 0;
}

int Cribbage::score_hand_doubles(vector<struct card> &hand){
// returns number of points earned from pairs in a hand

	int points;
	int numofCards=hand.size();
	int i;
	int j;
	int pairs=0;
	vector <int> paired;

	// For loops to look for doubles
	for(i=0; i<numofCards; i++){
		for(j=i; j<numofCards-1; j++){
			if(hand[i].rank==hand[j+1].rank && already_paired(j, j+1, paired)==0){
			// If not already a stored pair
				pairs=pairs+1;
			}
		}
	}

	points = pairs*2;
	return points;
}


int Cribbage::hand_score_runs(vector<struct card> &hand){
// returns the number of points gained from runs in a hand

	vector<int> runHand;		// dummy vector to sort and evaluate for a run
	int runs = 0;				// for keeping track of the number of run
	int run_length = 1;
	int multiplier = 1;
	int points = 0;

	for(int i = 0; i<hand.size(); i++){
		runHand.push_back(hand[i].rank);			// steps through, add ranks to dummy vector
	}

	sort(runHand.begin(),runHand.end());			// sorts dummy vector of all the ranks

	for(int j = 0; j<(runHand.size()-1); j++){
			if((runHand[j]+1)==runHand[j+1]){		// if next card is only one apart...
				run_length++;						// increment length (originally 1)
				if(runs<run_length){				// IF it's a record length...
					runs = run_length;				// set runs to run_length
				}
			}
			if(runHand[j]==runHand[j+1]){			// if next card is equal...
				multiplier++;						// increments multiplier
			}
			if((runHand[j]+1)!=runHand[j+1] && runHand[j]!=runHand[j+1]){	// Otherwise...
				run_length = 1;
			}
	}

	if(runs>=3){		// If there's enough in a row to make a run...
		points = runs*multiplier;
	}
	
	runHand.clear();								// empty dummy vector for next time
	return points;
}


int Cribbage::hand_score_flush(vector<struct card> &hand){
// Returns the number of points gained from flushes in the hand
// at least 4 cards of the same suit

	int numofCards=hand.size();
	int i;
	int flushscore=0;

	int hearts=0;
	int spades=0;
	int clubs=0;
	int diamonds=0; 

	for(i=0; i<numofCards; i++)
	{
		if(hand[i].suit == 'H')
			{
				hearts=hearts+1;
			}
		else if(hand[i].suit == 'S')
			{
				spades=spades+1;
			}
		else if(hand[i].suit == 'C')
			{
				clubs=clubs+1;
			}
		else if(hand[i].suit == 'D')
			{
				diamonds=diamonds+1;
			}
	}

	if(hearts>4)
		{
			flushscore=flushscore+hearts;
		}
	if(spades>4)
		{
			flushscore=flushscore+spades;
		}
	if(diamonds>4)
		{
			flushscore=flushscore+diamonds;
		}
	if(clubs>4)
		{
			flushscore=flushscore+clubs;
		}	
	return flushscore;
}



int Cribbage::score_hand_starter(vector<struct card> &hand){
// Returns number of points for jacks of the same suit as the starter card

	int size = hand.size();
	int jacks = 0;
	for(int i = 0; i<(size-1); i++){		// DON'T check last card! (Because it IS the starter card...)
		if(hand[i].rank==11 && hand[i].suit==starter[0].suit){		// If it's a jack and the suits are the same
			jacks++;
		}
	}
	return jacks;
}

void Cribbage::scoreHands(){
// Scores the hands at the end of the turn

	// DISPLAY CRIB AND HANDS
	printCrib();
	DisplayHands(1);
	DisplayHands(2);

	for(int i=0; i<8; i++) {		// Erases card from play hand
		FillRect(cardplay_x[i],cardplay_y[i],106,169,1);
	}
	SDL_Flip(screen);			// Applies to screen

	// ADDS STARTER CARD TO CRIB AND BOTH HANDS
	(*playerHand).push_back(starter[0]);
	(*dealerHand).push_back(starter[0]);
	crib.push_back(starter[0]);

	// get score from player's hand
	int pS_15 = score_fifteens(*playerHand);
	int pS_doub = score_hand_doubles(*playerHand);
	int pS_run = hand_score_runs(*playerHand);
	int pS_flush = hand_score_flush(*playerHand);
	int pS_start = score_hand_starter(*playerHand);

	// get score from dealer's hand
	int dS_15 = score_fifteens(*dealerHand);
	int dS_doub = score_hand_doubles(*dealerHand);
	int dS_run = hand_score_runs(*dealerHand);
	int dS_flush = hand_score_flush(*dealerHand);
	int dS_start = score_hand_starter(*dealerHand);

	// get score from dealer's crib
	int cS_15 = score_fifteens(crib);
	int cS_doub = score_hand_doubles(crib);
	int cS_run = hand_score_runs(crib);
	int cS_flush = hand_score_flush(crib);
	int cS_start = score_hand_starter(crib);

	// DISPLAY MESSAGES FOR PLAYER'S SCORES

	if(pS_15!=0){								// non-dealer fifteens
		changeScore(playerScore,pS_15);
		score_message("Fifteens: ",pS_15,"Scoring hand for player ",player);	
	}
	if(pS_doub!=0){								// non-dealer pairs
	changeScore(playerScore,pS_doub);
		score_message("Pairs: ",pS_doub,"Scoring hand for player ",player);	
	}
	if(pS_run!=0){								// non-dealer runs
	changeScore(playerScore,pS_run);
		score_message("Runs: ",pS_run,"Scoring hand for player ",player);	
	}
	if(pS_flush!=0){							// non-dealer flush
	changeScore(playerScore,pS_flush);
		score_message("Flush: ",pS_flush,"Scoring hand for player ",player);	
	}
	if(pS_start!=0){							// non-dealer starter card
	changeScore(playerScore,pS_start);
		score_message("His nobs: ",pS_start,"Scoring hand for player ",player);	
	}
	if(score1>=maxScore || score2>=maxScore){
		endGame();								// endGame if maxScore reached
		
	}

	// DISPLAY MESSAGES FOR DEALER'S SCORES
	if(dS_15!=0){								// dealer fifteens
	changeScore(dealerScore,dS_15);				
		score_message("Fifteens: ",dS_15,"Scoring hand for player ",dealer);	
	}
	if(dS_doub!=0){								// dealer pairs
	changeScore(dealerScore,dS_doub);			
		score_message("Pairs: ",dS_doub,"Scoring hand for player ",dealer);	
	}
	if(dS_run!=0){								// dealer runs
	changeScore(dealerScore,dS_run);
		score_message("Runs: ",dS_run,"Scoring hand for player ",dealer);	
	}
	if(dS_flush!=0){							// dealer flush
	changeScore(dealerScore,dS_flush);
		score_message("Flush: ",dS_flush,"Scoring hand for player ",dealer);	
	}
	if(dS_start!=0){							// dealer starter card
	changeScore(dealerScore,dS_start);
		score_message("His nobs: ",dS_start,"Scoring hand for player ",dealer);	
	}
	if(score1>=maxScore || score2>=maxScore){
		endGame();								// endGame if maxScore reached
		
	}

	// DISPLAY MESSAGES FOR DEALER'S CRIB

	if(cS_15!=0){								// dealer crib fifteens
		changeScore(dealerScore,cS_15);
		score_message("Fifteens: ",pS_15,"Scoring crib for player ",dealer);	
	}
	if(cS_doub!=0){								// dealer crib pairs
		changeScore(dealerScore,cS_doub);
		score_message("Pairs: ",cS_doub,"Scoring crib for player ",dealer);	
	}
	if(cS_run!=0){								// dealer crib runs
		changeScore(dealerScore,cS_run);
		score_message("Runs: ",cS_run,"Scoring crib for player ",dealer);	
	}
	if(cS_start!=0){							// dealer crib starter card
		changeScore(dealerScore,cS_start);
		score_message("His nobs: ",cS_start,"Scoring crib for player ",dealer);	
	}

	// scoring flush for the crib
	if(cS_flush==5){							// flush only counts in crib if ALL are the same suit as the starter
		changeScore(dealerScore,cS_flush);
		score_message("Flush: ",cS_flush,"Scoring crib for player ",dealer);
	}	
	if(score1>=maxScore || score2>=maxScore){
		endGame();								// endGame if maxScore reached
		
	}
}

void Cribbage::update_board(int score1, int score2){

	int SecondRowXconv;
	int player1xpos, player1ypos, player2xpos, player2ypos, player1row2Y, player1row1Y, player2row1Y, player2row2Y;
	int xwinner, ywinner;
	xwinner = 544;
	ywinner = 689;
	SecondRowXconv=15*31; // Conversion factor for second row
	SDL_Flip( screen );
	player1row1Y=650; // Ypos for 1st player 1st row
	player1row2Y=729; // Ypos for 2nd player 1st row
	player2row1Y=666; // Ypos for 1st player 2nd row
	player2row2Y=745; // Ypos for 2nd player 2nd row

	int xStart=80; // starting x position for each row

	apply_surface (30, 600, board, screen  );

	if(score1<=30) // If statement for 1st row
	{
		player1xpos=xStart+score1*15-15;
		player1ypos=player1row1Y;
		apply_surface(player1xpos, player1ypos, peg1, screen); 
	}
	if(score1>30 && score1<61) // If statement for 2nd row 
	{
		player1xpos=xStart+(score1*15)-SecondRowXconv;
		player1ypos=player1row2Y;
		apply_surface(player1xpos, player1ypos, peg1, screen); 
	}
	if(score1>=61) // If statement for winner
	{
		player1xpos=xwinner;
		player1ypos=ywinner;
		apply_surface(player1xpos, player1ypos, peg1, screen); 
	}
	if(score2<=30) // If statement for 1st row
	{
		player2xpos=xStart+score2*15-15;
		player2ypos=player2row1Y;
		apply_surface(player2xpos, player2ypos, peg2, screen); 
	}
	if(score2>30 && score2<61) // If statement for 2nd row
	{
		player2xpos=xStart+score2*15-SecondRowXconv;
		player2ypos=player2row2Y;
		apply_surface(player2xpos, player2ypos, peg2, screen); 
	}
	if(score2>=61) // If statement for winner
	{
		player2xpos=xwinner;
		player2ypos=ywinner;
		apply_surface(player2xpos, player2ypos, peg2, screen); 
	}

	SDL_Flip( screen );

}				


void Cribbage::changeScore(int *score, int change){
// Increments the score by <change> amount of points
	
	(*score)+=change;
	update_board(score1, score2);	// Loads updated score on the board
	SDL_Flip(screen);		// Updates screen

	FillRect(200,815,100,100,1);			// cover up old score with black rectangle
	stringstream score1str;
	score1str<<score1;
	string str1 = score1str.str();
	char* score1_str = (char*)str1.c_str();
	load_message(score1_str,200,815,1,40);	// display player 1 score

	FillRect(475,815,100,100,1);			// cover up old score with black rectangle
	stringstream score2str;
	score2str<<score2;
	string str2 = score2str.str();
	char* score2_str = (char*)str2.c_str();
	load_message(score2_str,485,815,1,40);	// display player 2 score
}

//Function to manage ALL buttons, to figure out which one was pressed!
int Cribbage::click_card(){
	// Defines holders for mouse coordinates
	int x=0, y=0;

	// While the mouse is not clicked
	while(1){
		// If mouse is clicked
		if(SDL_PollEvent(&event)){
			// Gets mouse coordinates
			x=event.motion.x;
			y=event.motion.y;

			if( event.type == SDL_MOUSEBUTTONDOWN ) {
				if(event.button.button==SDL_BUTTON_LEFT){
					// Checks which (if any) button was pressed
					if(hand1_0.was_pressed(x,y)){
						return 0;
					} else if (Cribbage::hand1_1.was_pressed(x,y)){
						return 1;
					} else if (Cribbage::hand1_2.was_pressed(x,y)){
						return 2;
					} else if (Cribbage::hand1_3.was_pressed(x,y)){
						return 3;
					} else if (Cribbage::hand1_4.was_pressed(x,y)){
						return 4;
					} else if (Cribbage::hand1_5.was_pressed(x,y)){
						return 5;
					} else if (Cribbage::hand2_0.was_pressed(x,y)){
						return 0;
					} else if (Cribbage::hand2_1.was_pressed(x,y)){
						return 1;
					} else if (Cribbage::hand2_2.was_pressed(x,y)){
						return 2;
					} else if (Cribbage::hand2_3.was_pressed(x,y)){
						return 3;
					} else if (Cribbage::hand2_4.was_pressed(x,y)){
						return 4;
					} else if (Cribbage::hand2_5.was_pressed(x,y)){
						return 5;
					} else {
						// Does nothing
					}
				}
			}
		}
	}
}

void Cribbage::endGame(){
// displays winner message, ends game

		const char* winner_message;
		if(score1>score2){								// determine winner
			winner_message = "Player 1 wins!";	
		}
		else{
			winner_message = "Player 2 wins!";
		}
		load_message(winner_message,300,475,1,130);		// display winner
		SDL_Delay(2000);
		exit(1);										// exit program
}

